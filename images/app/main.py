import base64

import uvicorn
from transformers import DetrImageProcessor, DetrForObjectDetection
import torch
from PIL import Image, ImageDraw
from fastapi import FastAPI, UploadFile
import io
import os


threshold = os.environ['THRESHOLD']
model_size = os.environ['MODEL_SIZE']


app = FastAPI()


@app.on_event("startup")
def startup_event():
    app.processor = DetrImageProcessor.from_pretrained("facebook/detr-resnet-" + model_size, revision="no_timm")
    app.model = DetrForObjectDetection.from_pretrained("facebook/detr-resnet-" + model_size, revision="no_timm")


@app.post("/detect", name = "Обнаружение объектов на изображении", tags = ["Object detection"])
def generate(file: UploadFile):
    image = Image.open(io.BytesIO(file.file.read())).convert('RGB')
    inputs = app.processor(images=image, return_tensors="pt")
    outputs = app.model(**inputs)

    target_sizes = torch.tensor([image.size[::-1]])
    results = app.processor.post_process_object_detection(outputs, target_sizes=target_sizes, threshold=float(threshold))[0]

    draw = ImageDraw.Draw(image)
    names = []

    for label, box in zip(results["labels"], results["boxes"]):
        box = [round(i, 2) for i in box.tolist()]
        names.append(app.model.config.id2label[label.item()])
        draw.rectangle(box, outline=(255, 0, 0), width=3)

    if len(names) > 0:
        buffered = io.BytesIO()
        image.save(buffered, format="JPEG")
        img_str = base64.b64encode(buffered.getvalue())
        return {"names": names, "image": img_str}

    return {"names": names}


if __name__ == "__main__":
    config = uvicorn.Config("main:app",
                            port=80,
                            host="0.0.0.0",
                            log_level="info",
                            workers=os.cpu_count())
    server = uvicorn.Server(config)
    server.run()
