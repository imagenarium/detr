<@requirement.NODE ref='detr' primary='detr-${namespace}' single='false' />

<@requirement.PARAM name='API_PORT' required='false' type='port' scope='global' />
<@requirement.PARAM name='THRESHOLD' type='select' values='0.9,0.8,0.7,0.6,0.5' value='0.8' scope='global' />
<@requirement.PARAM name='MODEL_SIZE' type='select' values='50,101' value='50' scope='global' />

<@img.TASK 'detr-${namespace}' 'imagenarium/detr:1.0'>
  <@img.NODE_REF 'detr' />
  <@img.VOLUME '/root/.cache' />
  <@img.PORT PARAMS.API_PORT '80' />
  <@img.ENV 'THRESHOLD' PARAMS.THRESHOLD />
  <@img.ENV 'MODEL_SIZE' PARAMS.MODEL_SIZE />
  <@img.CHECK_PORT '80' />
</@img.TASK>
